'use strict';

var gulp = require('gulp');

require('require-dir')('./gulp');

gulp.task('default', ['styles', 'html', 'scripts', 'browsersync'], function () {
    gulp.watch("app/assets/**/*.scss", ['styles']);
    gulp.watch("app/*.html", ['html']);
    gulp.watch("app/assets/partials/**/*.html", ['views']);
    gulp.watch("app/assets/scripts/**/*.js", ['scripts']);
});

gulp.task('build', ['styles', 'html', 'vendor', 'views', 'scripts'], function () {
});


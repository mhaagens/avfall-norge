var app = angular.module('avfallApp', ['ngRoute', 'ngAnimate', 'ngGrid']);

// Routing

app.config(['$routeProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/hjem.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge', {
            templateUrl: 'partials/omavfallnorge.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/generelt', {
            templateUrl: 'partials/omavfallnorge/generelt.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/styret', {
            templateUrl: 'partials/omavfallnorge/styret.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/ansatte', {
            templateUrl: 'partials/omavfallnorge/ansatte.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/kalender', {
            templateUrl: 'partials/omavfallnorge/kalender.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/medlemsbedrifter', {
            templateUrl: 'partials/omavfallnorge/medlemsbedrifter.html',
            controller: 'MedlemsbedrifterCtrl'
        }).
        when('/omavfallnorge/aktuellavfallspolitikk', {
            templateUrl: 'partials/omavfallnorge/aktuellavfallspolitikk.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/rapporter', {
            templateUrl: 'partials/omavfallnorge/rapporter.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/rapporter/rapport', {
            templateUrl: 'partials/omavfallnorge/rapport.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/avfallnorgemener', {
            templateUrl: 'partials/omavfallnorge/avfallnorgemener.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/arbeidsgrupper', {
            templateUrl: 'partials/omavfallnorge/arbeidsgrupper.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/arbeidsgrupper/arbeidsgruppe', {
            templateUrl: 'partials/omavfallnorge/arbeidsgruppe.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/internasjonaltarbeid', {
            templateUrl: 'partials/omavfallnorge/internasjonaltarbeid.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/informasjonformedia', {
            templateUrl: 'partials/omavfallnorge/informasjonformedia.html',
            controller: 'MainCtrl'
        }).
        when('/omavfallnorge/kontaktoss', {
            templateUrl: 'partials/omavfallnorge/kontaktoss.html',
            controller: 'MainCtrl'
        }).
        when('/nyheter', {
            templateUrl: 'partials/omavfallnorge/nyheter.html',
            controller: 'MainCtrl'
        }).
        when('/artikkelmal', {
            templateUrl: 'partials/artikkelmal.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen', {
            templateUrl: 'partials/ombransjen/ombransjen.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/generelt', {
            templateUrl: 'partials/ombransjen/generelt.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/tallogfakta', {
            templateUrl: 'partials/ombransjen/tallogfakta.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/lovverk', {
            templateUrl: 'partials/ombransjen/lovverk.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/lokaleavfallsforum', {
            templateUrl: 'partials/ombransjen/lokaleavfallsforum.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/bransjenyheter', {
            templateUrl: 'partials/ombransjen/bransjenyheter.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/bransjekalender', {
            templateUrl: 'partials/ombransjen/bransjekalender.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/stillinger', {
            templateUrl: 'partials/ombransjen/stillinger.html',
            controller: 'MainCtrl'
        }).
        when('/ombransjen/stillinger/stilling', {
            templateUrl: 'partials/ombransjen/stilling.html',
            controller: 'MainCtrl'
        }).
        when('/arrangementer', {
            templateUrl: 'partials/arrangementer/arrangementer.html',
            controller: 'MainCtrl'
        }).
        when('/arrangementer/arrangement', {
            templateUrl: 'partials/arrangementer/arrangement.html',
            controller: 'MainCtrl'
        }).
        when('/arrangementer/andrearrangementer', {
            templateUrl: 'partials/arrangementer/andrearrangementer.html',
            controller: 'MainCtrl'
        }).
        when('/arrangementer/dokumentasjon', {
            templateUrl: 'partials/arrangementer/dokumentasjon.html',
            controller: 'MainCtrl'
        }).
        when('/fagomraader', {
            templateUrl: 'partials/fagomraader/fagomraader.html',
            controller: 'MainCtrl'
        }).
        when('/fagomraader/forskningogutvikling', {
            templateUrl: 'partials/fagomraader/forskningogutvikling.html',
            controller: 'MainCtrl'
        }).
        when('/fagomraader/deponering', {
            templateUrl: 'partials/fagomraader/deponering.html',
            controller: 'MainCtrl'
        }).
        when('/blimedlem', {
            templateUrl: 'partials/blimedlem.html',
            controller: 'MainCtrl'
        }).
        when('/logginn', {
            templateUrl: 'partials/logginn.html',
            controller: 'MainCtrl'
        }).
        when('/logginnmobil', {
            templateUrl: 'partials/logginnmobil.html',
            controller: 'MainCtrl'
        }).
        when('/paamelding', {
            templateUrl: 'partials/paamelding.html',
            controller: 'MainCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
]);

// PHONE CONTROLLERS

app.controller('MainCtrl', function($scope, $http, $location) {

    $scope.orderProp = 'age';
    $scope.isActive = function(route) {
        return route === $location.path();
    }
});

// FAGOMRAADER

app.controller('FagomraaderListCtrl', function($scope, $http, $location) {

    $scope.isActive = function(route) {
        return route === $location.path();
    }
});

app.controller('FagomraaderDetailCtrl', ['$scope', '$routeParams', '$http',
]);

app.controller('MedlemsbedrifterCtrl', function($scope, $http, $location) {
    $scope.gridOptions = {
        data: 'myData',
        maxWidth: 9000,
        enablePinning: true,
        columnDefs: [{
            field: "Navn",
            width: 250,
            pinned: true
        }, {
            field: "Sted",
            width: 120
        }, {
            field: "Fylke",
            width: 120
        }, {
            field: "Telefon",
            width: 120
        }, {
            field: "Epost",
            width: 120
        }]
    };
    $scope.myData = [{
        Navn: "Accon AS",
        Sted: "Tønsberg",
        Fylke: "Vestfold",
        Telefon: "33359303",
        Epost: "info@accon.no",
    }, {
        Navn: "Agder Renovasjon",
        Sted: "Arendal",
        Fylke: "Aust-Agder",
        Telefon: "37058800",
        Epost: "kundesenter@agderrenovasjon.no",
    }, {
        Navn: "Agenturhuset Salg",
        Sted: "Oslo",
        Fylke: "Oslo",
        Telefon: "22262321",
        Epost: "kontor@agenturhuset.no",
    }, {
        Navn: "Aquateam COWI AS",
        Sted: "Oslo",
        Fylke: "Oslo",
        Telefon: "22358100",
        Epost: "aquateam@aquateam.no",
    }, {
        Navn: "Asker kommune",
        Sted: "Asker",
        Fylke: "Akershus",
        Telefon: "66909000",
        Epost: "post@asker.kommune.no",
    }, {
        Navn: "Asplan Viak AS",
        Sted: "Sandvika",
        Fylke: "Akershus",
        Telefon: "67525200",
        Epost: "sandvika@asplanviak.no",
    }, {
        Navn: "Aurskog-Høland kommune",
        Sted: "Bjørkelangen",
        Fylke: "Akershus",
        Telefon: "63852500",
        Epost: "postmottak@ahk.no",
    }, {
        Navn: "Avfall Sør Husholdning",
        Sted: "Kristiansand S",
        Fylke: "Vest-Agder",
        Telefon: "38177070",
        Epost: "post@avfallsor.no",
    }];

    $scope.isActive = function(route) {
        return route === $location.path();
    }
});

// Off Canvas Fix

$(document).ready(function() {
    $(".navmenu").click(function() {
        $('.navmenu').offcanvas('hide')
    });
});

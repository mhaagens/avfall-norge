var gulp        = require('gulp');
    browserSync = require('browser-sync');
    reload      = browserSync.reload;

// browser-sync task for starting the server.
gulp.task('browsersync', function() {
    browserSync({
        server: {
            baseDir: "dist/",
            port: 3000
        },
        notify: false
    });
});
var gulp = require('gulp');

gulp.task('views', function () {
  return gulp.src('app/assets/partials/**/*.html')
    .pipe(gulp.dest('dist/partials'))
    .pipe(reload({stream:true}));
});
var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;

gulp.task('scripts', function() {
  gulp.src([
    'app/assets/scripts/controllers.js'
    ])
    .pipe(concat('app.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(reload({stream:true}));
});
var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
  size = require('gulp-size');

gulp.task('vendor', function() {
  gulp.src([
  	'app/assets/bower/jquery/dist/jquery.min.js',
    'app/assets/bower/bootstrap-sass-official/assets/javascripts/bootstrap.js',
    'app/assets/bower/jasny-bootstrap/js/offcanvas.js',
  	'app/assets/bower/angular/angular.min.js',
    'app/assets/bower/angular-resource/angular-resource.js',
    'app/assets/bower/angular-route/angular-route.min.js',
    'app/assets/bower/angular-animate/angular-animate.min.js',
    'app/assets/bower/ng-grid/build/ng-grid.min.js'
  	])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(size());
});
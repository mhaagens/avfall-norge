'use strict';

var gulp = require('gulp'),
	compass = require('gulp-compass'),
	autoprefixer = require('gulp-autoprefixer'),
	size = require('gulp-size'),
	reload      = browserSync.reload;

gulp.task('styles', function () {
  return gulp.src('app/assets/styles/**/*.scss')
    .pipe(compass({
      css: '.tmp',
      sass: 'app/assets/styles'
    }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
    .pipe(gulp.dest('.tmp'))
    .pipe(size())
    .pipe(gulp.dest('dist/css/'))
    .pipe(reload({stream:true}));
});